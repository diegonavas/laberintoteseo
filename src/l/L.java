/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author DIEGO NAVAS U
 */
public class L {

    short matriz[][];
    short distancias[][];
    int veces = 0;
    
    public L(String txtUrl) {
        java.util.ArrayList<String> l = new java.util.ArrayList<String>();
        try {
            URL url = new URL(txtUrl);
            BufferedReader in = null;
            try {
                in = new BufferedReader(new InputStreamReader(
                        url.openStream()));
            } catch (Throwable t) {
            }
            String inputLine;

            int alto = 0, datos = 0;
            String[] splited = null;
            while ((inputLine = in.readLine()) != null) {
                splited = inputLine.split(";");
                for (int i = 0; i < splited.length; i++) {
                    l.add(splited[i]);
                }
                alto++;
            }
            this.matriz = new short[alto][splited.length];
            for (int i = 0; i < alto; i++) {
                for (int j = 0; j < splited.length; j++) {
                    this.matriz[i][j] = Short.parseShort(l.get(datos));
                    datos++;
                }
            }
            //IMPRESION DEL LABERINTO
            for (int i = 0; i < alto; i++) {
                for (int j = 0; j < splited.length; j++) {
                    System.out.print("     " + matriz[i][j] + "    ");
                }
                System.out.println("");
            }

            in.close();

        } catch (Exception ex1) {
            System.out.println("URL erronea: " + ex1.getMessage());
        }
    }
   /*llena la matriz con distancias hasta que encuentre al minotauro
    cm es la distancia entre la casilla actual a la casilla de Teseo*/
    public int getT(int fTeseo, int cTeseo, int cm, int e) {
        this.veces++;
        if (fTeseo < 0 || fTeseo >= matriz.length) {
            return 999999;//retorna un numero muy  grande si se sale de las filas
        }
        if (cTeseo < 0 || cTeseo >= matriz[fTeseo].length) {
            return 999999;//retorna un numero muy grande si se sale de las columnas
        }
        if (matriz[fTeseo][cTeseo] == -1) {
            distancias[fTeseo][cTeseo] = 8888;//establece un numero muy grande para las distancias de las paredes
            return 9999;//y luego retorna otro numero muy grande para que ignore esa pared
        }
        if (matriz[fTeseo][cTeseo] == 100) {
            matriz[fTeseo][cTeseo] *= -1;//mata  al minotauro
            distancias[fTeseo][cTeseo] = (short) (cm);//establece su distancia
            return 1;
        }
        if (distancias[fTeseo][cTeseo] < cm) {
            return 1;//valida que no hayan calculado esa distancia antes
        }
        distancias[fTeseo][cTeseo] = (short) Math.min(distancias[fTeseo][cTeseo], cm);
        /*estados 
        0=primera vez, 1 = subiendo
        2 = bajando, 3 = izquierda
        4 = derecha
        esto se hace porque no tiene sentido dar pasos hacia atras respecto a la ruta que llevo
         */
        if (e == 1) {
            return Math.min(Math.min(getT((fTeseo - 1), cTeseo, cm + 1, 1), getT(fTeseo, (cTeseo - 1), cm + 1, 3)), getT(fTeseo, (cTeseo + 1), (cm + 1), 4)) + 1;
        }
        if (e == 2) {
            return Math.min(Math.min(getT(fTeseo + 1, cTeseo, cm + 1, 2), getT(fTeseo, cTeseo - 1, cm + 1, 3)), getT(fTeseo, cTeseo + 1, cm + 1, 4)) + 1;
        }
        if (e == 3) {
            return Math.min(Math.min(getT(fTeseo - 1, cTeseo, cm + 1, 1), getT(fTeseo + 1, cTeseo, cm + 1, 2)), getT(fTeseo, cTeseo - 1, cm + 1, 3)) + 1;
        }
        if (e == 4) {
            return Math.min(Math.min(getT(fTeseo - 1, cTeseo, cm + 1, 1), getT(fTeseo + 1, cTeseo, cm + 1, 2)), getT(fTeseo, cTeseo + 1, cm + 1, 4)) + 1;
        }
        if (e == 0) {
             return Math.min(Math.min(getT(fTeseo - 1, cTeseo, cm + 1, 1), getT(fTeseo + 1, cTeseo, cm + 1, 2)), Math.min(getT(fTeseo, cTeseo - 1, cm + 1, 3), getT(fTeseo, cTeseo + 1, cm + 1, 4))) + 1;
        }
        return 0;
    }
    //Este metodo es el encargado de mostrar el camino (las coordenadas) entre Teseo y el Minotauro.
    public String getC(int fTeseo, int cTeseo) {
        this.veces++;
        int num = distancias[fTeseo][cTeseo];//guardamos la distancia de esa fila,columna
        //valida si puedo subir
        if (fTeseo > 0) {
            //pregunto que celda tiene una distancia menor a la que estoy - 1
            if (distancias[fTeseo - 1][cTeseo] == num - 1) {
                matriz[fTeseo - 1][cTeseo] = 1;//coloco el uno  en la matriz de respuesta,esto se repite en las otras posibilidades(if)
                return getC(fTeseo - 1, cTeseo) + " _(" + fTeseo + "-" + cTeseo + ")";//retorno la recursion + la posicion actual como string
            }
        }
         //de aqui hacia abajo se repite el proceso solo que cambia las validaciones segun sea el caso
        if (fTeseo < distancias.length - 1) {
            if (distancias[fTeseo + 1][cTeseo] == num - 1) {
                matriz[fTeseo + 1][cTeseo] = 1;
                return getC(fTeseo + 1, cTeseo) + " _(" + fTeseo + "-" + cTeseo + ")";
            }
        }
        if (cTeseo > 0) {
            if (distancias[fTeseo][cTeseo - 1] == num - 1) {
                matriz[fTeseo][cTeseo - 1] = 1;
                return getC(fTeseo, cTeseo - 1) + " _(" + fTeseo + "-" + cTeseo + ")";
            }
        }
        if (cTeseo < distancias[fTeseo].length - 1) {
            if (distancias[fTeseo][cTeseo + 1] == num - 1) {
                matriz[fTeseo][cTeseo + 1] = 1;
                return getC(fTeseo, cTeseo + 1) + " _(" + fTeseo + "-" + cTeseo + ")";
            }
        }
        return "_(" + fTeseo+"-"+cTeseo+")";
    }

    public void reiniciarDistancias(int x, int y) {
        
        distancias[x][y] = 9999;//inicia la matriz de distancias en la posicion [x][y] con un num muy grande
        // System.out.print(distancias[x][y]+" "+x+" - "+y);
        
        //valida que aun hayan columnas
        if (y < distancias[x].length - 1) {
            reiniciarDistancias(x, y + 1);//hace la recursion para la siguiente columna
        }
        //valida que aun hayan filas
        else if (x < distancias.length - 1) {
            //pasa al comienzo de la siguiente fila
            reiniciarDistancias(x + 1, 0);

        }
    }

    /*
        Este metodo muestra el camino a partir de la posicion del minotauro hasta la salida.
    */
    public String getCs(int f, int c) {
        
        this.veces++;
        int num = distancias[f][c];
        if (f > 0) {
            if (distancias[f - 1][c] == num - 1) {
                matriz[f - 1][c] = 1;
                return getC(f - 1, c) + " _(" + f + "-" + c + ")";
            }
        }
        if (f < distancias.length - 1) {
            if (distancias[f + 1][c] == num - 1) {
                matriz[f + 1][c] = 1;
                return getCs(f + 1, c) + " _(" + f + "-" + c + ")";
            }
        }
        if (c > 0) {
            if (distancias[f][c - 1] == num - 1) {
                matriz[f][c - 1] = 1;
                return getCs(f, c - 1) + " _(" + f + "-" + c + ")";
            }
        }
        if (c < distancias[f].length - 1) {
            if (distancias[f][c + 1] == num - 1) {
                matriz[f][c + 1] = 1;
                return getCs(f, c + 1) + " _(" + f + "-" + c + ")";
            }
        }
        return "";
    }
    /*llena la matriz con distancias desde el minotauro hacia la salida
    cm es la distancia entre la casilla actual a la casilla de Salida*/
    public int getTs(int f, int c, int cm, int e) {
         
        this.veces++;
        if (f < 0 || f > matriz.length - 1) {
            return 999999;
        }
        if (c < 0 || c > matriz[f].length - 1) {
            return 999999;
        }
        if (matriz[f][c] == -1) {
            distancias[f][c] = 8888;
            return 999999;
        }
        if (matriz[f][c] == 1000) {
            distancias[f][c] = (short) (cm);
            return 1;
        }
        if (distancias[f][c] < cm) {
            return 0;
        }
        distancias[f][c] = (short) Math.min(distancias[f][c], cm);
        
        if (e == 1) {
            return Math.min(Math.min(getTs((f - 1), c, cm + 1, 1), getTs(f, (c - 1), cm + 1, 3)), getTs(f, (c + 1), (cm + 1), 4)) + 1;
        }
        if (e == 2) {
            return Math.min(Math.min(getTs(f + 1, c, cm + 1, 2), getTs(f, c - 1, cm + 1, 3)), getTs(f, c + 1, cm + 1, 4)) + 1;
        }
        if (e == 3) {
            return Math.min(Math.min(getTs(f - 1, c, cm + 1, 1), getTs(f + 1, c, cm + 1, 2)), getTs(f, c - 1, cm + 1, 3)) + 1;
        }
        if (e == 4) {
            return Math.min(Math.min(getTs(f - 1, c, cm + 1, 1), getTs(f + 1, c, cm + 1, 2)), getTs(f, c + 1, cm + 1, 4)) + 1;
        }
        if (e == 0) {
            return Math.min(Math.min(getTs(f - 1, c, cm + 1, 1), getTs(f + 1, c, cm + 1, 2)), Math.min(getTs(f, c - 1, cm + 1, 3), getTs(f, c + 1, cm + 1, 4))) + 1;
        }
        return 1;
    }

    public static void main(String[] args) {
        // TODO code application logic here
        
        L l = new L("https://gitlab.com/madarme/archivos-persistencia/raw/master/laberinto/matrizLaberintoTeseo.txt");
        //ArchivoLeerURL a = new ArchivoLeerURL("https://gitlab.com/madarme/archivos-persistencia/blob/master/laberinto/matrizLaberintoTeseo.txt");
        l.distancias = new short[l.matriz.length][l.matriz[0].length];

        for (int i = 0; i < l.distancias.length; i++) {
            for (int j = 0; j < l.distancias[i].length; j++) {
                l.distancias[i][j] = 9999;
            }
        }
        
        int a = l.getT(8, 1, 1, 0);//posicion de teseo (fTeseo, cTeseo, 1, 0)
        
        //System.out.println(a + " - a");
        System.out.print("Camino de Teseo: ");
        System.out.print(l.getC(4, 0));//posicion minotauro (fMinotauro, cMinotauro)

        l.reiniciarDistancias(0, 0);
        int v = l.getTs(4, 0, 1, 0); //posicion minotauro (fMinotauro, cMinotauro, 1, 0)

        System.out.print("-"+l.getCs(1, 4));//posicion salida (fSalida, colSalida) 
       
        System.out.println("");
        System.out.println("Matriz Resultante");
        
        for (int i = 0; i < l.matriz.length; i++) {
            for (int j = 0; j < l.matriz[i].length; j++) {
                System.out.print(l.matriz[i][j] + "        //       ");
            }
            System.out.println("");
        }
        
        System.out.println("cantidad de movimientos : " +l.veces);

    }

}

